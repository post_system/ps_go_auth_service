module upm/udevs_go_auth_service

go 1.16

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/bxcodec/faker/v3 v3.8.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.9.1
	github.com/golang/protobuf v1.5.3
	github.com/google/uuid v1.5.0
	github.com/jackc/pgx/v4 v4.18.1
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/pkg/errors v0.9.1
	github.com/spf13/cast v1.6.0
	github.com/streamingfast/logging v0.0.0-20230608130331-f22c91403091
	github.com/stretchr/testify v1.8.4
	github.com/swaggo/files v1.0.1
	github.com/swaggo/gin-swagger v1.6.0
	github.com/swaggo/swag v1.16.2
	go.uber.org/zap v1.21.0
	golang.org/x/crypto v0.17.0
	google.golang.org/grpc v1.60.1
	google.golang.org/protobuf v1.32.0
)
